----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.10.2022 16:02:34
-- Design Name: 
-- Module Name: reg_N - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity reg_N is
port ( clk, rst, en:in std_logic;
       d: in std_logic_vector(7 downto 0);
       q: out std_logic_vector(7 downto 0));
end reg_N;


architecture archi of reg_N is
begin
process (rst, clk)
  begin 
     if rst='1' then q <= (others => '0');
     
     elsif clk'event and clk='1' then
        if en='1' then 
            q <= d ;
        end if;
     end if;
end process;
end archi;