----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.10.2022 16:22:24
-- Design Name: 
-- Module Name: dff_rs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity dff_rs is
port (clk:in std_logic;
      d: in std_logic;
      rst : in std_logic;
      q: out std_logic);
end dff_rs;

architecture archi of dff_rs is--- redefinition de dff pour son utlisation

--component dff
--port (clk:in std_logic;
--      d: in std_logic;
--      q: out std_logic);
--end component;

begin
process (clk)
    begin 
        if rst='1' and clk='1' then
            q<='0';
        elsif clk'event and clk='1' then
            q <= d;
        end if;
end process;

    
end archi;

