library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity compteur_8 is
 Port (clk , rst, en: in std_logic;
        q: out std_logic_vector(7 downto 0)
       );
end compteur_8;

architecture Behavioral of compteur_8 is

signal q_int: std_logic_vector(7 downto 0);  

begin
 process(clk, rst) 
 begin
    if rst='1' then q_int <= (others => '0');
     elsif clk'event and clk='1' then
      if en='1' then
        q_int <= q_int+1;
      end if;
    end if;
 end process;
 q <= q_int;

end Behavioral;
