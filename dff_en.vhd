----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.10.2022 15:45:08
-- Design Name: 
-- Module Name: dff_en - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity dff_en is
port ( clk ,d, rst, en : in std_logic;
      q: out std_logic);
end dff_en;

architecture archi of dff_en is--- redefinition de dff pour son utlisation

--component dff
--port (clk:in std_logic;
--      d: in std_logic;
--      q: out std_logic);
--end component;

begin
process (rst, clk)
  begin 
     if rst='1' then q<='0';
     
     elsif clk'event and clk='1' then
        if en='1' then 
            q <= d;
        end if;
     end if;
end process;

    
end archi;