----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.09.2022 16:24:56
-- Design Name: 
-- Module Name: addN - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity addN is
generic(N: integer := 4);
port ( a,b: in std_logic_vector ( N-1 downto 0);
 s: out std_logic_vector (N-1 downto 0));
end addN;
architecture archi of addN is
begin
--process(a,b)
--begin
s <= a + b ;
--end process ;
end archi;
