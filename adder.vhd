----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.09.2022 17:17:24
-- Design Name: 
-- Module Name: adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity adder is
port( a,b: in std_logic_vector ( 3 downto 0);
      s: out std_logic_vector ( 3 downto 0);
      cf, ovf: out std_logic);
end adder;

architecture archi of adder is
begin
process(a,b)
 variable temp: std_logic_vector(4 downto 0); 
begin
    temp := ( '0' & a ) + ( '0' & b ) ; --concat�nation : on met 0 � la gauche de a et pareil pour b (initialement 4 bits chacuns) on a 2 vect de 5 bits
    s <= temp (3 downto 0) ; --on affecte � s les 4 bits de poids faible de temp
    cf <= temp(4) ;
    ovf <= temp(3) xor a(3) xor b(3) xor temp(4) ;
end process;
end archi;
-------------;
